import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Video from './views/Video.vue'
import Collection from './views/Collection.vue'
import Setting from './views/Setting.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {name: "图片"}
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
      meta: { name: "关于" }      
    },
    {
      path: '/collection',
      name: 'collection',
      component: Collection,
      meta: { name: "收藏" }   
    },
    {
      path: '/video',
      name: 'video',
      component: Video,
      meta: { name: "视频" }  
    },
    {
      path: '/setting',
      name: 'setting',
      component: Setting,
      meta: { name: "设置" } 
    },
  ]
})
