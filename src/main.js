import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'vant/lib/vant-css/index.css';
import { Field, Row, Col, Uploader, ImagePreview, Dialog, Toast, NavBar , Lazyload, List, Cell } from 'vant';
import VueLazyload from 'vue-lazyload'
import './assets/font/iconfont.css'

Vue.config.productionTip = false
Vue.use(Lazyload);
Vue.use(Toast);
Vue.use(List);
Vue.use(Cell);
Vue.use(NavBar);
Vue.use(ImagePreview);
Vue.use(Uploader);
Vue.use(Row);
Vue.use(Col);
Vue.use(Dialog);
Vue.use(VueLazyload);
Vue.use(Field);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
